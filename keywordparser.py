#!/usr/bin/python27
from argparse import ArgumentParser 
from argparse import ArgumentError
import time
import json

def main():
    parser = ArgumentParser(description='Please input what you would like to do.')
    parser.add_argument('-e', '--exclude', action='append', nargs='+', 
                help="Exclude files with the keyword [arg1, arg2 ...]", 
                metavar=('keyword'))
    parser.add_argument('-i', '--include', action='append', nargs='+', 
                help="include files with the keyword [arg1, arg2 ...]", 
                metavar=('keyword'))
    parser.add_argument('-l', '--idenpercent', action='append', nargs=1, 
                help="Throwout entries that do not meet the [arg] minimum identity:align-length ratio.", 
                metavar=('identity_percentage'))
    parser.add_argument('-q', '--lenpercent', action='append', nargs=1, 
                help="Throwout entries that do not meet the [arg] minimum align-length:query-length ratio.", 
                metavar=('length_percentage'))
    parser.add_argument('-f', '--filename', action='append', nargs=1, required=True, 
                help="REQUIRED Name of the blast file that we would like to parse.",
                metavar=('filename'))
    parser.add_argument('-o', '--output', action='append', nargs=1,
                help ="Name of where you would like to save the output. the first line of output contains the total length of the query and the number of hits that were thrown out.",
                metavar=('output'))
    #change this to support text or contigs, that way the user can look at either one, if contigs contigs arg is required
    parser.add_argument('-t', '--outputtype', action='append',nargs=1,
                help ="Type of output you would like (html, json, text).",
                metavar=('outputtype'))
    #ident length
    parser.add_argument('-y', '--minidentity', action='append',nargs=1,
                help ="Filters the results by the minimum identity length given.",
                metavar=('minidentity'))
    #threads This is sort of hard right now and will be placed back a bit on priority
    parser.add_argument('-p', '--threads', action='append',nargs=1,
                help ="Number of threads that you would like to use. A Number that is too high may cause a slowdown.",
                metavar=('threads'))
    #contigs file.
    # if hit then copy from the contigs.fa into keyword_contigs_date|out.fa 
    # copy whole iteration and make sure there are no repeats
    # Note this is a whole new parsing problem and will also be difficult
    parser.add_argument('-c', '--contigfile', action='append',nargs=1,
                help = "Locations of the fasta file that we are working with. If this command appears then a new\
                contigs file will be created along with the output. The file will share the name as -o but with the .fa extension",
                metavar=('contigfile'))
    
    
    #Parses the user input.
    try:
        args = parser.parse_args()
    except:
        return
    arg_size = 0
    filter_list = {}
    idenr = 0
    lenr = 0
    idenl = 0
    output = "keywordparsed_results_" + str(time.time())

    filename = None
    contigs = False
    contigsf = None
    if args.idenpercent != None:
        idenr = args.idenpercent[0][0]
        arg_size += 1
        
    if args.lenpercent != None:
        lenr = args.lenpercent[0][0]
        arg_size += 1
        
    if args.filename != None:
        filename = args.filename[0][0]
        arg_size += 1
        
    if args.exclude != None:
        for i in args.exclude[0]:
            filter_list[i] = False
        arg_size += 1
        
    if args.include != None:
        for i in args.include[0]:
            filter_list[i] = True
        arg_size += 1
    
    if args.output != None:
        output = args.output[0][0]
    
    if args.minidentity != None:
        idenl = args.minidentity[0][0]
    
    if args.outputtype != None:
        outputtype = args.outputtype[0][0]
    else:
        outputtype = "text"
    if args.contigfile != None:
        contigs = True
        contigsf = args.contigfile[0][0]
    
    fd = open(filename, "r")
    xmlparser = ListXMLParser(fd, filter_list,  idenr = idenr, lenr = lenr , idenl = idenl)
    xmlparser.hit_reader()
    if contigs != True:
        xmlparser.display_results(filename, output, outputtype)
    else:
        xmlparser.build_fasta(contigsf, output + ".fa")
        xmlparser.display_results(filename, output, outputtype)
    #format the output somehow here
    fd.close()
    print "parsing:", xmlparser.time_counting_hits, "filtering:",xmlparser.time_filtering_hits, "reading:",xmlparser.time_reading_hits
        
class ListXMLParser():
    lines = []
    query_length = None
    hits_thrown_out = None
    #list of hit_info
    hits = []
    identity_ratio_lim = None
    length_ratio_lim = None
    identity_minimum = None
    #just so i have a reference to look at
    hit_info = {
                 "hit_id":None, 
                 "hit_def":None,
                 "evalue":None,
                 "align-len":None,
                 "q-sequence":None,
                 "h-sequence":None,
                 "identity":None,
                 "identity:align-len":None,
                 "align-len:query-len":None,
                 "iteration_def":None
                }
    filter_rules = {}
    #contigfs file location OR false
    
    #length ratio = length/query length
    #identity:align-lenn = identity/align-length
    def __init__(self, filehandle, filter_list = {}, idenr = 0, lenr = 0, idenl = 0):
        lines = filehandle.readlines()
        for line in lines:
            self.lines.append(line.strip())
        self.query_length = self._inner_text(self.lines[9])
        self.hits_thrown_out = 0
        self.filter_rules = filter_list
        self.identity_ratio_lim = idenr
        self.length_ratio_lim = lenr
        self.identity_minimum = idenl
        self.contigs_used = {}
        self.time_counting_hits = 0.0
        self.time_filtering_hits = 0.0
        self.time_reading_hits = 0.0
    
        
    def hit_reader(self):
        cur = None
        iteration_def = None
        for i in range(len(self.lines)):
            start = time.time()
            cur = self.lines[i]
            #<Iteration_query-def>NODE_1_length_661_cov_9.184568</Iteration_query-def>
            if "<Iteration_query-def>" in cur:
                start = cur.find(">") + 1
                end = cur[start:].find("<") + start
                iteration_def = cur[start:end]
            if cur == "<Hit>":
                #28 if gaps exists, 27 if no gaps exist, gaps is at 20
                hit = self.lines[i:i+28]
                if "gaps" in hit[20]:
                    self.time_reading_hits += time.time() - start
                    self.process_hit(hit, iteration_def)
                else:
                    hit = self.lines[i:i+27]
                    self.time_reading_hits += time.time() - start
                    self.process_hit(hit, iteration_def)
            else:
                pass    
    
    def process_hit(self, hit, iteration_def):
        hit_dic = {}
        hit_dic['hit_def'] = self._inner_text(hit[3])
        if not self.filter_hit_def(hit_dic['hit_def'].lower()):
            self.hits_thrown_out += 1
            return
        start = time.time()
        hit_dic['hit_id'] = self._inner_text(hit[2])
        hit_dic['evalue'] = self._inner_text(hit[11])
        hit_dic['identity'] = self._inner_text(hit[18])
        self.contigs_used[iteration_def] = True
        if len(hit) == 28:    
            hit_dic['align-len'] = self._inner_text(hit[21])
            hit_dic['q-sequence'] = self._inner_text(hit[22])
            hit_dic['h-sequence'] = self._inner_text(hit[23])
        else:
            hit_dic['align-len'] = self._inner_text(hit[20])
            hit_dic['q-sequence'] = self._inner_text(hit[21])
            hit_dic['h-sequence'] = self._inner_text(hit[22]) 
                   
        #Does our value checks, probably doesn't belong here though.
        #avoid integer division, catch divide by zero
        try:
            hit_dic['identity:align-len'] = (int(hit_dic['identity']) + 0.0)/(int(hit_dic['align-len']))
        except:
            self.hits_thrown_out += 1
            for lines in hit:
                print lines
            print " "
            return
        if self.identity_minimum > hit_dic['identity']:
            self.hits_thrown_out+=1
            return
        #don't bother catching divide by zero here.. catch it on startup
        try:
            hit_dic['align-len:query-len'] = (int(hit_dic['align-len']) + 0.0)/ int(self.query_length)
            if self.identity_ratio_lim > hit_dic['identity:align-len'] \
					or self.length_ratio_lim > hit_dic['align-len:query-len']:
                self.hits_thrown_out+=1
                return
            self.hits.append(hit_dic)
            self.time_counting_hits += time.time() - start
            #save contig if it doesn't exist
        except:
            print "Caught ValueError with:"
            print hit
            return
    #===========================================================================
    # Function runs the word against our set of rules and will return false if it does not match any of the rules
    #
    # returns true if we should keep the word and false if we should not keep the word
    #===========================================================================
    def filter_hit_def(self, hit_def ):
        start = time.time()
        for key in self.filter_rules:
            #if the word doesnt exist and we expect it to, return false
            if hit_def.find(key.lower()) == -1 and self.filter_rules[key] == True:
                self.time_filtering_hits += time.time() - start
                return False
            #if the word does exist and we don't want it to, return false
            if hit_def.find(key.lower()) > -1 and self.filter_rules[key] == False:
                self.time_filtering_hits += time.time() - start
                return False
        #if we pass every test return true
        self.time_filtering_hits += time.time() - start
        return True
        
    def _inner_text(self, sec):
        start = sec.find('>') + 1
        end = sec.find('<', start)
        return sec[start:end]
              
    # type can be, text, html, json
    def display_results(self, filename, outfilename = None, type = "text"):
        out = open(outfilename, 'w')
        if type == "text":
            #create header:
            out.write(filename + " Query Length: " + str(self.query_length) + " Hits thrown out: " +  str(self.hits_thrown_out) + '\n' )
            out.write("RESULTS:\n")
            for item in self.hits:
                out.write("HIT\n")
                for key in item:
                    out.write("\t"+key+": "+str(item[key])+"\n")
            out.close()
        elif type == "html":
            #create header:
            out.write("<html><title>"+outfilename+"</title>\n")
            out.write("<strong>Filename</strong>"+filename + "<br/><strong>Query Length:</strong> " + str(self.query_length) + "<br/><strong>Hits thrown out: </strong>" +  str(self.hits_thrown_out) + '<br/>' )
            out.write("<h3>RESULTS</h3>")
            for item in self.hits:
                out.write("<table>")
                for key in item:
                    out.write("<tr><td>"+key+"</td><td>"+str(item[key])+"</td></tr>")
                out.write("</table><p/>")
            out.close()
        elif type == "json":
            header = {"filename": outfilename, "Query Length": self.query_length, "Hits Thrown Out": self.hits_thrown_out}
            out.write(json.dumps(header, skipkeys=True, indent=1, separators=(',', ': ')))
            for item in self.hits:
                out.write(json.dumps(item, skipkeys=True, indent=1, separators=(',', ': ')))
            out.close()
    def print_lines(self):
        for line in self.lines:
            print line
    
    #===============================================================================
    # Creates a new fasta file with the contigs that only appear in our query. 
    # We read from the input file and save to a file with the same name and the ending _filtered_time
    # Read part by part (making this parallel will make this easier) if dictionary[contigid] write.
    # Writing needs to be synchronized 
    # synchronize later in build_fasta_synchronized, everything is going to have to be written differently
    #===============================================================================
    def build_fasta(self, fastaf, outputf):
        # 6million lines is easily storable by our computer. don't worry aobut that. so store ALL of the used headers when doing the first section.
        # work machine can store all of the lines in memory without getting to 20% mem
        # this is EXTREMELY important.
        #needs to be done in parallel... without breaking memory lol
        # read line => contig
        # check if line is in our other struct, if so write to new file, if not pass, maybe we want to be passed lines so it's easier to 
        try:
            fd = open(fastaf, 'r')
            out = open(outputf, 'w')
            save = False
            contig = []
            print self.contigs_used
            for line in fd:
                #is header
                if ">" in line:
                    #flush contig
                    if len(contig) != 0:
                        #asynchronious will check lock
                        out.writelines(contig)
                        contig = []
                    #check usage
                    if line[1:].strip() in self.contigs_used:
                        #mark to save lines until next header
                        save = True
                        contig.append(line)
                    else:
                        save = False
                #reading DNA line and is eligible to write
                elif save == True:
                    contig.append(line)
        finally:
            fd.close()
            out.close()
if __name__ == "__main__":
    main()
